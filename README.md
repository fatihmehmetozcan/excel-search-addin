This is inspired by https://github.com/JoshuaFlynn/Searcher , but I was not comfortable modifying it to my needs because of code structure so I had to start from scratch. Form and some elements' names might be similar.


<hr>


Overview of user form


![1](/Inst/Inst1.png)


<hr>


### Installation


Right click on any tab and go to customize


![2](/Inst/Inst2.png)


Enable 'Developer' tab


![3](/Inst/Inst3.png)


Then go to Add-Ins


![4](/Inst/Inst4.png)


Browse and select the [.xlam](/excel-search-addin.xlam) file


![5](/Inst/Inst5.png)


Create a new tab and group. After that select 'Macros' from dropdown list and add 'searchTool' to the group

 
![6](/Inst/Inst6.PNG)


And you can click the button and use the tool


![7](/Inst/Inst7.png)